package org.academiadecodigo.bootcamp;



import org.academiadecodigo.bootcamp.World.Blocks.Block;
import org.academiadecodigo.bootcamp.World.Blocks.BlockFactory;
import org.academiadecodigo.bootcamp.Player.Player;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;


public class Game {
    public static final int PADDING = 10;
    public static final int BLOCK_SIZE = 128;
    public Block[] level1;
    public Rectangle newBg;
    public Block[][] blocks;


    private Player player;

    public Game(Player player){
        this.player = player;





    }
    public void init(){
        this.newBg = new Rectangle(PADDING,PADDING,700,700);
        newBg.setColor(Color.LIGHT_GRAY);
        newBg.fill();

        BlockFactory blockFactory = new BlockFactory(20);
        this.blocks = blockFactory.createFloor( newBg);

        this.level1 = blockFactory.createLevel1(newBg);

        for (int row = 0; row < blocks.length; row++) {
            for (int col = 0; col < blocks[row].length; col++) {
                blocks[row][col].drawBlock();
            }
        }








        Text keybinds = new Text(300,60,"A - Left   ||   D - Right   ||   W - stop");
        keybinds.setColor(Color.BLACK);

        keybinds.draw();

        Text directionText = new Text(100,70,"0!");
        directionText.setColor(Color.BLACK);

        directionText.draw();

        KeyboardHandler keyBoardHandler = new GameKeyboardHandler(player, directionText);
        Keyboard k = new Keyboard(keyBoardHandler);

        KeyboardEvent d_press = new KeyboardEvent();
        d_press.setKey(KeyboardEvent.KEY_D);
        d_press.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        k.addEventListener(d_press);

        KeyboardEvent d_release = new KeyboardEvent();
        d_release.setKey(KeyboardEvent.KEY_D);
        d_release.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
        k.addEventListener(d_release);

        KeyboardEvent a_press = new KeyboardEvent();
        a_press.setKey(KeyboardEvent.KEY_A);
        a_press.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        k.addEventListener(a_press);

        KeyboardEvent a_release = new KeyboardEvent();
        a_release.setKey(KeyboardEvent.KEY_A);
        a_release.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
        k.addEventListener(a_release);
        KeyboardEvent w_press = new KeyboardEvent();
        w_press.setKey(KeyboardEvent.KEY_W);
        w_press.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        k.addEventListener(w_press);

        KeyboardEvent w_release = new KeyboardEvent();
        w_release.setKey(KeyboardEvent.KEY_W);
        w_release.setKeyboardEventType(KeyboardEventType.KEY_RELEASED);
        k.addEventListener(w_release);
    }
    public void start() throws InterruptedException {

        while(true){

            if(player.getOldDirection() != player.getDirection())
            {
                player.stopAll(player.getOldDirection());
                // TODO: 6/9/19 directional idleRight effect!
                player.setOldDirection(player.getDirection());
            }

            player.move(player.getDirection());

/*            for(int i=0; i<blocks.length; i++)
            {
                for(int j=0; j<blocks[i].length; j++){

                    blocks[i][j].drawBlock();
                }
            }*/

            for(Block b : level1){

                // TODO: 6/10/19 BUGGED!

                b.move(player.getDirection());
                if(newBg.getWidth()-32 >= b.getX()){
                    b.drawBlock();
                }else if(b.getX()+32 >newBg.getWidth()-32){
                    b.deleteBlock();
                }
                //System.out.println(newBg.getWidth()-32 + "getX : "+b.getX() );
            }
        }
    }
}
