package org.academiadecodigo.bootcamp;


import org.academiadecodigo.bootcamp.Player.Player;

public class Main {
    public static void main(String[] args) throws InterruptedException {

        Player player1 = new Player();

        Game g = new Game(player1);
        g.init();
        g.start();
    }

}
