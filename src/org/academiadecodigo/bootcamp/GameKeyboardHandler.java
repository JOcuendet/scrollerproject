package org.academiadecodigo.bootcamp;

import org.academiadecodigo.bootcamp.Player.Player;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;


public class GameKeyboardHandler implements KeyboardHandler {
    private final Text directi;
    Player player1;

    public GameKeyboardHandler(Player player, Text directi) { // pass by class.
        this.player1 = player;
        this.directi = directi;


    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        int key = keyboardEvent.getKey();

        switch (key){
            case KeyboardEvent.KEY_A:
                player1.setDirection(DirectionType.RIGHT);
                //directi.setText(DirectionType.RIGHT.name());
                //directi.draw();
                break;
            case KeyboardEvent.KEY_D:
                player1.setDirection(DirectionType.LEFT);
                //directi.setText(DirectionType.LEFT.name());
                //directi.draw();
                break;
            case KeyboardEvent.KEY_W:
                player1.setDirection(DirectionType.JUMP);

                break;


        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
        int key = keyboardEvent.getKey();

        switch (key) {
            case KeyboardEvent.KEY_A:
                player1.setDirection(DirectionType.IDLELEFT);
                break;
            case KeyboardEvent.KEY_D:
                player1.setDirection(DirectionType.IDLERIGHT);
                break;
            case KeyboardEvent.KEY_W:
                player1.setDirection(player1.getOldDirection());
        }

    }
}
