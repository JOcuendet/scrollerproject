package org.academiadecodigo.bootcamp.Player;

import org.academiadecodigo.bootcamp.DirectionType;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Player {
    private Picture[] jumpLoopStills;
    private Picture[] walkLeftStills;
    private Picture[] idleRightStills;
    private Picture[] walkRightStills;
    private Picture[] idleLeftStills;

    private DirectionType direction;
    private DirectionType oldDirection;
    private final int X;
    private final int Y;

    private int health;
    private int bullets;
    private int bulletDamage;

    public Player(){
        this.health = 100;              // TODO: 09/06/2019 MY SHIIIIIIIIIIIIIIIT
        this.bullets = 999999999;
        this.bulletDamage = 25;
        this.X = 150;
        this.Y = 410;
        this.direction = DirectionType.IDLERIGHT;
            this.oldDirection = DirectionType.IDLERIGHT;

        /**
         * Create and load @walkLeftStills Animation Still Images
         */
        Picture[] walkLeftStills = new Picture[5];
        String pathLeft = "./Assets/Player/Walking/";

        for (int i=0; i<walkLeftStills.length;i++){
        Picture newWalkLeftAnimation = new Picture(X,Y,pathLeft +"0_Fallen_Angels_Walking_"+ i +".png");
        walkLeftStills[i] = newWalkLeftAnimation;
        }

        this.walkLeftStills = walkLeftStills;


        /**
         * Create and load @idleRightStills Animation Still Images
         */

        Picture[] idleRightStills = new Picture[4];
        String pathIdle = "./Assets/Player/IdleBlinking/";

        for (int i=0; i<idleRightStills.length;i++){
            Picture newIdleRightAnimation = new Picture(X,Y,pathIdle +"0_Fallen_Angels_IdleBlinking_"+ i +".png");
            idleRightStills[i] = newIdleRightAnimation;
        }
        this.idleRightStills = idleRightStills;


        /**
         * Create and load @walkRightStills Animation Still Images
         */
        Picture[] walkRightStills = new Picture[5];
        String pathRight = "./Assets/Player/WalkingRight/";

        for (int i=0; i<walkRightStills.length;i++){
            Picture newWalkRightAnimation = new Picture(X,Y,pathRight +"0_Fallen_Angels_Walking_"+ i +".png");
            walkRightStills[i] = newWalkRightAnimation;
        }
        this.walkRightStills = walkRightStills;


        /**
         * Create and load @idleLeftStills Animation Still Images
         */
        Picture[] idleLeftStills = new Picture[4];
        String pathIdleLeft = "./Assets/Player/IdleLeftBlinking/";

        for (int i=0; i<idleLeftStills.length;i++){
            Picture newIdleLeftAnimation = new Picture(X,Y,pathIdleLeft +"0_Fallen_Angels_IdleBlinking_"+ i +".png");
            idleLeftStills[i] = newIdleLeftAnimation;
        }

        this.idleLeftStills = idleLeftStills;

        /**
         * Create and load @idleLeftStills Animation Still Images
         */
        Picture[] jumpLoopStills = new Picture[5];
        String pathJumpLoop = "./Assets/Player/JumpLoop/";

        for (int i=0; i<jumpLoopStills.length;i++){
            Picture newJumpAnimation = new Picture(X,Y,pathJumpLoop +"0_Fallen_Angels_JumpLoop_"+ i +".png");
            jumpLoopStills[i] = newJumpAnimation;
        }

        this.jumpLoopStills = jumpLoopStills;
    }

    public void setDirection(DirectionType direction) {
        this.direction = direction;
    }

    public DirectionType getDirection() {
        return direction;
    }

    public DirectionType getOldDirection() {
        return oldDirection;
    }

    public void setOldDirection(DirectionType oldDirection) {
        this.oldDirection = oldDirection;
    }

    public void move(DirectionType direction) throws InterruptedException {

        switch(direction){
            case LEFT:
                walkLeft();
                break;
            case RIGHT:
               walkRight();
                break;
            case IDLELEFT:
                idleLeft();
                break;
            case IDLERIGHT:
                idleRight();
                break;
            case JUMP:
                jump();
                break;
        }
    }

    public void idleRight() throws InterruptedException{

        if(direction == DirectionType.IDLERIGHT)
        {
            for(int i = 0; i< idleRightStills.length; i++){
                Thread.sleep(50);
                if(i>0)
                    idleRightStills[i-1].delete();

                idleRightStills[i].draw();
                Thread.sleep(50);
                if(i==idleRightStills.length-1){
                    idleRightStills[i].delete();
                    idleRightStills[0].draw();
                }

            }
        }

    }

    public void idleLeft() throws InterruptedException{

        if(direction == DirectionType.IDLELEFT)
        {
            for(int i = 0; i< idleLeftStills.length; i++){
                Thread.sleep(50);
                if(i>0)
                    idleLeftStills[i-1].delete();

                idleLeftStills[i].draw();
                Thread.sleep(50);
                if(i==idleLeftStills.length){
                    idleLeftStills[i].delete();
                    idleLeftStills[0].draw();
                }
            }
        }
    }

    public void walkLeft() throws InterruptedException { // add speed variable for "walkLeft" run!

        for(int i = 0; i< walkLeftStills.length; i++){
            if(direction == DirectionType.LEFT)
            {
                Thread.sleep(41);
                if(i>0)
                    walkLeftStills[i-1].delete();

                walkLeftStills[i].draw();
                Thread.sleep(41);
                if(i==walkLeftStills.length-1){
                    walkLeftStills[i].delete();
                    walkLeftStills[0].draw();

                }
            }
        }

    }

    public void walkRight() throws InterruptedException { // add speed variable for "walkLeft" run!

        for(int i = 0; i< walkRightStills.length; i++){
            if(direction == DirectionType.RIGHT)
            {
                Thread.sleep(41);
                if(i>0)
                    walkRightStills[i-1].delete();

                walkRightStills[i].draw();
                Thread.sleep(41);
                if(i==walkRightStills.length-1){
                    walkRightStills[i].delete();
                    walkRightStills[0].draw();

                }
            }
        }

    }

    public void jump() throws InterruptedException{
        for(int i = 0; i< jumpLoopStills.length; i++){
            if(direction == DirectionType.JUMP)
            {
                Thread.sleep(41);
                if(i>0)
                    jumpLoopStills[i-1].delete();

                jumpLoopStills[i].draw();
                Thread.sleep(41);
                if(i==jumpLoopStills.length-1){
                    jumpLoopStills[i].delete();
                    jumpLoopStills[0].draw();

                }
            }
        }
    }


    public void getHit(int damage){

        health -= damage;
    }

    // TODO: 09/06/2019 MY SHIIIIIIIIIIT
    public void addHeal(int healer){

        int tooMuchHp = health + healer;

        if (health == 100){

            System.out.println("You have full health already.");
            return ;
        }

        if((tooMuchHp) >= 100){

            health += (100 - health);
            System.out.println("Player hp2: " + health);
            return;
        }

        health += healer;
        System.out.println("Player hp3: " + health);
    }                                // TODO: 09/06/2019 MY SHIIIIIIIIIIIIIIT

    public int showHealth(){

        System.out.println("benficaEnemy health: " + health);
        return health;
    }




    public void stopAll(DirectionType oldDirectionStopper){
        switch(oldDirectionStopper){
            case IDLERIGHT:
                for(int i = 0; i< idleRightStills.length; i++) {
                    idleRightStills[i].delete();
                }
                break;
            case IDLELEFT:
                for(int i = 0; i< idleLeftStills.length; i++) {
                    idleLeftStills[i].delete();
                }
                break;
            case LEFT:
                for(int i = 0; i< walkLeftStills.length; i++){
                    walkLeftStills[i].delete();
                }
                break;
            case RIGHT:
                for(int i = 0; i< walkRightStills.length; i++){
                    walkRightStills[i].delete();
                }
                break;
            case JUMP:
                for(int i = 0; i< jumpLoopStills.length; i++){
                    jumpLoopStills[i].delete();
                }
                break;
        }
    }
}
