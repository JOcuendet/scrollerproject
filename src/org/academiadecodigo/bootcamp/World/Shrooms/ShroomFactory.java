package org.academiadecodigo.bootcamp.World.Shrooms;

public class ShroomFactory {

    public Shroom[] shrooms;

    public ShroomFactory(int numOfShrooms){
                                            // TODO: 09/06/2019 fazer shrooms para dar vida.
        shrooms = new Shroom[numOfShrooms];

        for (int i = 0; i < shrooms.length; i++){

            shrooms[i] = new Shroom();
        }
    }

    public Shroom getShroom(int shroom){

        return shrooms[shroom];
    }
}
