package org.academiadecodigo.bootcamp.World.Shrooms;

import org.academiadecodigo.bootcamp.Player.Player;

public class Shroom {
                                                // TODO: 09/06/2019 ganhar hp ao longo do mapa
    private int healer;
    private boolean hasHealer;

    public Shroom(){

        this.healer = 50;
        this.hasHealer = true;
    }

    public void heal(Player player){

        if(hasHealer == false){

            System.out.println("empty shroom");
            return;
        }

        if((player.showHealth() + healer) > 100){

            player.addHeal(healer - (player.showHealth() - healer));
            healer = 0;
            hasHealer = false;
        }
            player.addHeal(healer);
            healer = 0;
            hasHealer = false;
    }
}
