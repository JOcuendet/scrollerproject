package org.academiadecodigo.bootcamp.World.Blocks;

import org.academiadecodigo.bootcamp.Utility;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class BlockFactory {
    private final int MAXBLOCKS;
    private final int FLOOR_LINE = 550;

    public BlockFactory(int MAXBLOCKS){
        this.MAXBLOCKS = MAXBLOCKS;
    }

    public Block[][] createFloor(Rectangle bg){

        int bgWidth = (int) Math.nextUp((bg.getWidth() / 32)+1);
        int bgHeight = (int) Math.nextUp((bg.getHeight()-FLOOR_LINE) / 32)+1;
        Block [][] floor = new Block[bgHeight][bgWidth];

        int x = 10;
        int y = 550;
        int xx = x;
        int yy = y;
        String bgImagePath = "";


        for (int row = 0; row < floor.length; row++) {
            for (int col = 0; col < floor[row].length; col++) {
                if(y == FLOOR_LINE){
                    bgImagePath = "./Assets/Blocks/top_floor.jpg";
                }
                else{
                    bgImagePath = "./Assets/Blocks/fillerImage.jpg";
                }
                floor[row][col] = new Block(xx, y, bgImagePath);
                xx+=32;
            }
            xx = x;
            y+=32;
        }

       return floor;
    }



    public Block[] createLevel1(Rectangle bg){
        int spaceSize = 32;
        int maxCellingY = 100;
        int maxFloorY = 410 - spaceSize;
        int bgWidth = bg.getWidth();
        int multiplier = Utility.GetRandomInt(4, 0);
        int xValue = bgWidth + (spaceSize * multiplier);

        int verticalPosition = Utility.GetRandomInt(maxFloorY, maxFloorY);
        Block[] level1 = new Block[20];

        for (int i =0; i<level1.length; i++){
            level1[i] = new Block(xValue ,verticalPosition, "./Assets/Blocks/fillerImage.jpg");
        }
        return level1;

    }

}
