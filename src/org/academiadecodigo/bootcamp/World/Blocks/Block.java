package org.academiadecodigo.bootcamp.World.Blocks;

import org.academiadecodigo.bootcamp.DirectionType;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Block {
    private Picture background;
    private int X;
    private int Y;


    public int getX() {
        return X;
    }

    public int getY() {
        return Y;
    }

    public Block(int x, int y, String backgroundPath){

        this.X = x;
        this.Y = y;
        this.background = new Picture(x,y,backgroundPath);
    }

    public void drawBlock(){

        background.draw();
    }

    public void deleteBlock(){
        background.delete();
    }

    public void move(DirectionType direction) throws InterruptedException {

        switch(direction){
            case LEFT:
                X = X - 32;

                background.translate(-32,0);
                break;
            case RIGHT:
                X = X + 32;
                background.translate(32,0);

                break;
        }
    }
}
