package org.academiadecodigo.bootcamp;

public enum DirectionType {
    LEFT,
    RIGHT,
    IDLELEFT,
    IDLERIGHT,
    JUMP;
}
