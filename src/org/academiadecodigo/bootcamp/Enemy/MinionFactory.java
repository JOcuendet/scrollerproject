package org.academiadecodigo.bootcamp.Enemy;

public class MinionFactory {
    
    public Minion[] minions; // TODO: 09/06/2019 Minion ou Enemy factory?
    
    public MinionFactory(int numOfEnemies) {

        minions = new Minion[numOfEnemies];

        for (int i = 0; i < minions.length; i++) {

            // TODO: 09/06/2019 aqui entra a duvida se é minion factory ou enemy factory, meto um boss na ultima posiçao??? 
            minions[i] = new Minion();
        }
    }

}
