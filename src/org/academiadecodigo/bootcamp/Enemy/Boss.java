package org.academiadecodigo.bootcamp.Enemy;

abstract class Boss extends Enemy {

    public Boss(int health, int bulletDamage) {

        super(health, bulletDamage); // TODO: 09/06/2019 todos os bosses igualmente fortes ou variamos?
    }

    public abstract void message();
}
