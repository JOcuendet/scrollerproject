package org.academiadecodigo.bootcamp.Enemy;

import org.academiadecodigo.bootcamp.Player.Player;

public class Enemy {

    private int health;
    private int bullets;
    private int bulletDamage;

    public Enemy(int health, int bulletDamage){

        this.health = health;
        this.bulletDamage = bulletDamage;
    }

            public void hitPlayer(Player player){

            if(player.showHealth() <= 0){

                System.out.println("Player is dead already, move on bro...");
                return;
            }

            if((player.showHealth() - bulletDamage) <= 0){

                player.getHit(bulletDamage - (bulletDamage - player.showHealth()));
                bullets--;
                System.out.println("Player is dead MUHAHAHA ^^");
                return;
            }

                player.getHit(bulletDamage);
                bullets--;
            }

            public void enemyGetHit(int damage){

                health -= damage;
            }

            public int showHealth(){

                System.out.println("345Enemy health: " + health);
                return health;
            }

    }

